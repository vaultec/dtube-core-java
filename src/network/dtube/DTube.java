package network.dtube;

import java.io.IOException;

import io.ipfs.api.IPFS;

/**
 * This is the main class that would be called when externally used.
 * 
 * @author vaultec
 */
public class DTube {
	public static void main(String[] args) {
		IPFS ipfs = new IPFS("/dnsaddr/ipfs.infura.io/tcp/5001/https");
		try {
			System.out.println(ipfs.id());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
