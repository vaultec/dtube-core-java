package network.dtube;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;

import io.ipfs.api.cbor.CborObject;
import io.ipfs.api.cbor.Cborable;
import io.ipfs.cid.Cid;
import io.ipfs.multihash.Multihash;

/**
 * Decentralized IPFS objects for all.
 * 
 * ~~root hash is discerned when IPFSVideo is serialized into an IPLD object.~~ N/A
 * This hash can be referenced on blockchain as it contains all needed information to display a video.
 * 
 * @author vaultec
 * @implement v0.5
 */
public interface DObject extends Cborable {
	//public DirWrapper video;
	
	public enum DType {
		IPFSVideo,
		DVideo,
		Perium,
		DImage
	}
	
	public DType type();
	
	default int getsize() {
		return this.toCbor().toByteArray().length;
	}
	default Multihash hash() {
        return cid();
	}
	default Cid cid() {
		try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(this.toCbor().toByteArray());
            byte[] digest = md.digest();
            Multihash h = new Multihash(Multihash.Type.sha2_256, digest);
            return new Cid(1, Cid.Codec.DagCbor, h);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
	}
	
	
	//Wait on this one
	public class DVideo extends IPFSVideo {
		private static final long serialVersionUID = 7030714640220040702L;
		//DType type = DType.DVideo;
		
		
		
		
		
		//example 240p -> 1.4Gb 
		//HashMap<String, Long> videosize;
		
		
		
		
		/**
		 * 
		 * @param provider, defines what steemit/dtube url to create example; dtube.network/author/permlink
		 * @return
		 */
		public String getlink(String provider) {
			//this.type
			return "";
		}
		public CborObject toCbor() {
			
			SortedMap<CborObject, CborObject> map = new TreeMap<CborObject, CborObject>();
			//this.getClass().getDeclaredFields().
			//map.put(key, value);
			
			return this.self().toCbor(); //Temp solution for now.
		}
	}
	public class DImage implements DObject {
		private final DType type = DType.DImage;
		
		public Cid raw;
		
		Optional<String> alt_text = Optional.empty();
		Optional<Integer> height = Optional.empty();
		Optional<Integer> width = Optional.empty();
		
		@Override
		public CborObject toCbor() {
			SortedMap<CborObject, CborObject> map = new TreeMap<CborObject, CborObject>();
			map.put(new CborObject.CborString("type"), 
					new CborObject.CborString(this.type.name()));
			
			if(height.isPresent()) {
				map.put(new CborObject.CborString("height"), 
						new CborObject.CborLong(height.get().intValue()));
			} else {
				map.put(new CborObject.CborString("height"), 
						new CborObject.CborNull());
			}
			
			if(height.isPresent()) {
				map.put(new CborObject.CborString("width"), 
						new CborObject.CborLong(width.get().intValue()));
			} else {
				map.put(new CborObject.CborString("width"), 
						new CborObject.CborNull());
			}

			if(height.isPresent()) {
				map.put(new CborObject.CborString("alt_text"), 
						new CborObject.CborString("alt_text"));
			} else {
				map.put(new CborObject.CborString("alt_text"), 
						new CborObject.CborNull());
			}
			if(raw != null) {
				map.put(new CborObject.CborString("raw"), 
						new CborObject.CborMerkleLink(raw));
			}
			
			return new CborObject.CborMap(map);
		}

		@Override
		public DType type() {
			return type;
		}
		
	}
	
}
