package network.dtube;

import java.io.IOException;
import java.io.Serializable;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;

import io.ipfs.api.IPFS;
import io.ipfs.api.cbor.CborObject;
import io.ipfs.api.cbor.CborObject.CborLong;
import io.ipfs.api.cbor.CborObject.CborMap;
import io.ipfs.api.cbor.CborObject.CborMerkleLink;
import io.ipfs.api.cbor.CborObject.CborNull;
import io.ipfs.api.cbor.CborObject.CborString;
import io.ipfs.cid.Cid;
import io.ipfs.multihash.Multihash;


public class IPFSVideo implements DObject, Serializable {
	private static final long serialVersionUID = -5431375333730689996L;
	
	private final DType type = DType.IPFSVideo;
	
	public SortedMap<String, Multihash> video_index = new TreeMap<String, Multihash>();
	
	public Multihash snap;
	public Multihash sprite;
	
	//Do not implement any of this until later.
	Optional<String> description = Optional.empty();
	Optional<String> title = Optional.empty();
	Optional<Float> length = Optional.empty();
	
	transient Optional<SortedMap<String, Long>> size_index = Optional.empty();
	
	public DType type() {
		return type;
	}

	public CborObject toCbor() {
		SortedMap<CborObject, CborObject> map = new TreeMap<CborObject, CborObject>();
		
		//Video Links
		SortedMap<CborObject, CborObject> video_links = new TreeMap<CborObject,CborObject>();
		for(String e : video_index.keySet()) {
			video_links.put(new CborObject.CborString(e), 
					new CborObject.CborMerkleLink(this.video_index.get(e)));
		}
		
		map.put(new CborObject.CborString("video_index"), 
				new CborObject.CborMap(video_links));
		//Add type
		map.put(new CborObject.CborString("type"), 
				new CborObject.CborString(this.type.name()));
		//Description check;
		if(description.isPresent()) {
			map.put(new CborObject.CborString("description"), 
				new CborObject.CborString(description.get()));
		} else {
			map.put(new CborObject.CborString("description"), 
					new CborObject.CborNull());
		}
		//Add title
		if(title.isPresent()) {
			map.put(new CborObject.CborString("title"), 
				new CborObject.CborString(title.get()));
		} else {
			map.put(new CborObject.CborString("title"), 
					new CborObject.CborNull());
		}
		//Add length
		if(length.isPresent()) {
			map.put(new CborObject.CborString("length"), 
					new CborObject.CborString(title.get()));
		} else {
			map.put(new CborObject.CborString("length"), 
					new CborObject.CborNull());
		}
		
		
		return new CborObject.CborMap(map);
	}
	
	
	public IPFSVideo self() {
		return this;
	}
	
	public static IPFSVideo fromByteArray(byte[] bytes) {
		return fromCbor(CborObject.fromByteArray(bytes));
	}
	public static IPFSVideo fromCbor(CborObject cbor) {
		SortedMap<CborObject,CborObject> map = ((CborMap) cbor).values;
		IPFSVideo out = new IPFSVideo();
		
		SortedMap<CborObject, CborObject> video_index_cbor = 
				((CborMap) map.get(new CborObject.CborString("video_index"))).values;
		
		
		for(CborObject key : video_index_cbor.keySet()) {
			String str_key = ((CborString) key).value;
			Multihash multihash = ((CborMerkleLink) video_index_cbor.get(key)).target;
			out.video_index.put(str_key, multihash);
		}
		if(map.get(new CborString("description")) instanceof CborNull) {
			out.description = Optional.empty();
		} else {
			out.description = Optional.of(((CborString) map.get(new CborString("description"))).value);

		}
		if(map.get(new CborString("title")) instanceof CborNull) {
			out.title = Optional.empty();
		} else {
			out.title = Optional.of(((CborString) map.get(new CborString("title"))).value);
		}
		if(map.get(new CborString("length")) instanceof CborNull) {
			out.length = Optional.empty();
		} else {
			out.length = Optional.of((float)((CborLong) map.get(new CborString("length"))).value);

		}
		
		return out;
	}
	public static IPFSVideo fetchFromCid(Cid cid, IPFS ipfs) {
		try {
			return fromByteArray(ipfs.block.get(cid));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}